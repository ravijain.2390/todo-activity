QUnit.test( "test", function( assert ) {


   //Removing all the data from localstorage.
   editTask = false;   
   selectedTaskId = '';    
   localStorage.removeItem("TODOList");
   todoListArray = [];

   upsertTODOTest('Test TODO1', 'High', '2019-11-10');

   upsertTODOTest('Test TODO2', 'Low', '2019-11-12');

   upsertTODOTest('Test TODO3', 'Medium', '2019-11-09');

   upsertTODOTest('Test TODO4', 'High', '2019-11-08');

   upsertTODOTest('Test TODO5', 'High', '2019-11-10');

   upsertTODOTest('Test TODO6', 'Medium', '2019-10-25');
    
   upsertTODOTest('Test TODO7', 'Low', '2019-10-27');    

   //Added 7 task and count should be equal to 7.
   assert.ok( todoListArray.length === 7, "Total record must be 7" );

   
   //Removing single task. Passing task id from record no 3 in array.
   deleteTODO(todoListArray[2].id);

   //After removing the task result should be 6.
   assert.ok(todoListArray.length === 6, "Record must be 6 after deleting a record!" );

   //Edit existing record.
   editTask = true; //Setting editTask true so that edit condition get executed.
   selectedTaskId = todoListArray[2].id; //assigning taskId to selectedTaskId. So that function would easy to fetch record from array.
   upsertTODOTest('updateTODO', todoListArray[2].priority, todoListArray[2].dueDate);
   assert.ok( todoListArray[2].task === "updateTODO", "Task name must be 'updateTODO' after updating the record" );
   

 });

 //Insert and Edit Task records.
 function upsertTODOTest(taskName,priority,dueDate){
   document.getElementById('taskName').value = taskName;
   document.getElementById('priority').value = priority;
   document.getElementById('dueDate').value = dueDate;     
   saveNewTODO();  
 }

 function saveNewTODO() {

    var taskName = document.getElementById('taskName').value;
    var priority = document.getElementById('priority').value;
    var dueDate = document.getElementById('dueDate').value;
    var today = new Date();
    today.setHours(0,0,0,0)
    var dueDateTime = new Date(dueDate);

    if(taskName == null || taskName == ''){
        alert('Please enter a task name');
        return;
    }

    if(dueDate == null || dueDate == ''){
        alert('Please select a due date');
        return;
    }
    if(dueDateTime < today)
        {
            alert('Due date should not be less than today');
            return;
        }

    if(editTask){
        var selectedIndex = getSelectedTaskIndex(selectedTaskId);
        todoListArray[selectedIndex].task = taskName;
        todoListArray[selectedIndex].priority = priority;
        todoListArray[selectedIndex].dueDate = dueDate;
    }
    else {
            var task = {"id":  'task' + '_' + Math.random().toString(36).substring(2) + Date.now().toString(36) , "task": taskName ,"priority": priority,"dueDate": dueDate} ;
            todoListArray.push(task);
    }
}

function deleteTODO(id) {
    var confirmPopup = confirm('That does not seem like a good idea. Are you sure you want to do that?');
    if(confirmPopup){
        var taskIndex = getSelectedTaskIndex(id);
        if(taskIndex !== -1){
            todoListArray.splice(taskIndex,1);
        }
    }
}

function getSelectedTaskIndex(id) {
    var selectedTaskIndex = -1;
    for(var index = 0;index< todoListArray.length;index++){
        if(todoListArray[index].id === id){
            selectedTaskIndex = index;
            break;
        }
    }
    return selectedTaskIndex;
}