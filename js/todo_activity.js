var todoListArray = [];
var editTask = false;
var selectedTaskId = '';
var taskStatus = 'Active';

/*** function to create New TODO ***/
function newTODO() {

    document.getElementById('overlay').style.display = 'block';
    document.getElementById('popupDiv').style.display = 'block';
}

/*** function to close popup ***/
function closePopup() {
    document.getElementById('overlay').style.display = 'none';
    document.getElementById('popupDiv').style.display = 'none';
    editTask = false;
    clearTodoForm();
}

/*** function to show TODO list ***/
function showTodoList(todoArray)
{
    var editImage = '';
    var deleteImage = '';
    var taskRow = '';
    var taskRowArray = '';
    for(var todo=0;todo<todoArray.length;todo++)
        {
            
                 editImage = '<img src="images/edit_icon.png" alt="Edit" class="editIcon" onclick = "updateTODO(\'' + todoArray[todo].id+ '\')">';
                 deleteImage = '<img src="images/delete_icon.png" alt="Delete" class= "deleteIcon" onclick = "deleteTODO(\'' + todoArray[todo].id+ '\')">';
                 taskRowArray = "<tr><td>"+todoArray[todo].task+"</td><td>"+todoArray[todo].priority+"</td><td>"+todoArray[todo].dueDate+"</td><td class = 'paddingRight30'>"+ editImage + deleteImage + "</td></tr>";  
                 taskRow+= taskRowArray;
        }
    return taskRow;
}

/*** function to show TODO status ***/
function taskStatusList(currentTaskStatus) {
    var taskStatusArray = document.getElementsByClassName('taskStatusTab');
    for(var task = 0; task< taskStatusArray.length;task++) {
        var taskStatusChild = taskStatusArray[task].childNodes[0];
        taskStatusChild.classList = [];
    }
    currentTaskStatus.classList.add('active');
    taskStatus = currentTaskStatus.innerHTML;
    showFilterTaskList();
}

/*** function to save new TODO ***/
function saveNewTODO() {

    var taskName = document.getElementById('taskName').value;
    var priority = document.getElementById('priority').value;
    var dueDate = document.getElementById('dueDate').value;
    var today = new Date();
    today.setHours(0,0,0,0)
    var dueDateTime = new Date(dueDate);

    if(taskName == null || taskName == ''){
        alert('Please enter a task name');
        return;
    }

    if(dueDate == null || dueDate == ''){
        alert('Please select a due date');
        return;
    }
    if(dueDateTime < today)
        {
            alert('Due date should not be less than today');
            return;
        }

    if(editTask){
        var selectedIndex = getSelectedTaskIndex(selectedTaskId);
        todoListArray[selectedIndex].task = taskName;
        todoListArray[selectedIndex].priority = priority;
        todoListArray[selectedIndex].dueDate = dueDate;
    }
    else {
            var task = {"id":  'task' + '_' + Math.random().toString(36).substring(2) + Date.now().toString(36) , "task": taskName ,"priority": priority,"dueDate": dueDate} ;
            todoListArray.push(task);
    }
    
    storeTODODataLocally();
    clearTodoForm();
}

/*** function to reset TODO form ***/
function clearTodoForm() {
    document.getElementById('taskName').value = ''
    document.getElementById('priority').value = 'High';
    document.getElementById('dueDate').value = ''
}

/*** function to store TODO date locally ***/
function storeTODODataLocally() {
    var storeTODO = JSON.stringify(todoListArray);
    localStorage.removeItem("TODOList");
    localStorage.setItem("TODOList",storeTODO);
    getTODOList(false);
    closePopup();
}

/*** function to get the selected TODO index ***/
function getSelectedTaskIndex(id) {
    var selectedTaskIndex = -1;
    for(var index = 0;index< todoListArray.length;index++){
        if(todoListArray[index].id === id){
            selectedTaskIndex = index;
            break;
        }
    }
    return selectedTaskIndex;
}

/*** function to update TODO ***/
function updateTODO(id) {
    var selectedIndex = getSelectedTaskIndex(id);
    if(selectedIndex !== -1){
        selectedTaskId = id;
        document.getElementById('taskName').value = todoListArray[selectedIndex].task;
        document.getElementById('priority').value = todoListArray[selectedIndex].priority;
        document.getElementById('dueDate').value = todoListArray[selectedIndex].dueDate;
        document.getElementById('overlay').style.display = 'block';
        document.getElementById('popupDiv').style.display = 'block';    
        editTask = true;
    }
}

/*** function to get TODO list ***/
function getTODOList(initialLoading) {
    var taskRow = '';
    var locallySavedTODO = JSON.parse(localStorage.getItem("TODOList"));
    if(locallySavedTODO !== null){

        for(var i=0;i<locallySavedTODO.length;i++)
         {
             if(initialLoading)
                 {
                    todoListArray.push(locallySavedTODO[i]);
         }
                 }
        document.getElementById("taskList").innerHTML = showTodoList(todoListArray);  
        showFilterTaskList();
    }    
}

/*** function to show active and completed TODO list ***/
function showFilterTaskList(){
    var taskDueDate = null;
    var today = new Date();
    today.setHours(0,0,0,0)
    var activeTaskList = [];
    var completedTaskList = []; 
    document.getElementById("taskList").innerHTML = '';
    for(var i =0; i < todoListArray.length;i++)
    {
            var temp = todoListArray[i];
            taskDueDate = new Date(temp.dueDate)
            if(taskDueDate >= today)
            {
                 activeTaskList.push(temp);
            }
            else
            { 
                completedTaskList.push(temp);
            }
                  
    }
    if(taskStatus === 'Active')
        {
            document.getElementById("taskList").innerHTML = showTodoList(activeTaskList);   
        }
    else
        {
            document.getElementById("taskList").innerHTML = showTodoList(completedTaskList); 
        }
    
}

/*** function to delete TODO ***/
function deleteTODO(id) {
    var confirmPopup = confirm('That does not seem like a good idea. Are you sure you want to do that?');
    if(confirmPopup){
        var taskIndex = getSelectedTaskIndex(id);
        if(taskIndex !== -1){
            todoListArray.splice(taskIndex,1);
            storeTODODataLocally();
        }
    }
}


